﻿using NUnit.Framework;
using TestCalculator;
using System;

namespace CalculatorTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator cal;

        [SetUp]
        public void Init() => cal = new Calculator();


        [Test]
        public void Sum_2Plus5_7Returned()
        {

            // arrange
            int a = 2;
            int b = 5;
            int expected = 7;

            //act
            var actual = cal.Sum(a, b);

            //assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Sub_345Minus5_340Returned()
        {

            // arrange
            int a = 345;
            int b = 5;
            int expected = 340;

            //act
            var actual = cal.Sub(a, b);

            //assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [TestCase(5, 5, 25)]
        [TestCase(25, 7, 175)]
        public void MultTest(int a, int b, int expected)
        {

            //act
            var actual = cal.Mul(a, b);

            //assert
            Assert.That(actual, Is.EqualTo(expected));
        }


        [Test]
        public void Div_7Div0_ThrowEx()
        {

            // arrange
            int a = 7;
            int b = 0;

            //act
            //assert
            Assert.That(() => cal.Div(a, b), Throws.TypeOf<DivideByZeroException>());
        }
    }
}
